## lineage_m20lte-userdebug 13 TQ2A.230305.008.C1 eng.idk.20230330.163622 dev-keys
- Manufacturer: samsung
- Platform: universal7904
- Codename: m20lte
- Brand: samsung
- Flavor: lineage_m20lte-userdebug
- Release Version: 13
- Kernel Version: 4.4.177
- Id: TQ2A.230305.008.C1
- Incremental: eng.idk.20230330.163622
- Tags: dev-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: samsung/lineage_m20lte/m20lte:13/TQ2A.230305.008.C1/idk03301632:userdebug/dev-keys
- OTA version: 
- Branch: lineage_m20lte-userdebug-13-TQ2A.230305.008.C1-eng.idk.20230330.163622-dev-keys
- Repo: samsung/m20lte
