#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from m20lte device
$(call inherit-product, device/samsung/m20lte/device.mk)

PRODUCT_DEVICE := m20lte
PRODUCT_NAME := lineage_m20lte
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-M205F
PRODUCT_MANUFACTURER := samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="lineage_m20lte-userdebug 13 TQ2A.230305.008.C1 eng.idk.20230330.163622 dev-keys"

BUILD_FINGERPRINT := samsung/lineage_m20lte/m20lte:13/TQ2A.230305.008.C1/idk03301632:userdebug/dev-keys
