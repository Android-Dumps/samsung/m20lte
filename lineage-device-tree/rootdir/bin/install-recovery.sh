#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/13500000.dwmmc0/by-name/RECOVERY:31651840:f715c4ca15ac0bc13abd5b21588185059d1dbc62; then
  applypatch \
          --flash /vendor/etc/recovery.img \
          --target EMMC:/dev/block/platform/13500000.dwmmc0/by-name/RECOVERY:31651840:f715c4ca15ac0bc13abd5b21588185059d1dbc62 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
