#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_m20lte.mk

COMMON_LUNCH_CHOICES := \
    lineage_m20lte-user \
    lineage_m20lte-userdebug \
    lineage_m20lte-eng
